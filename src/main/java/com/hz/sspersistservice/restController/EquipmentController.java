package com.hz.sspersistservice.restController;

import com.hz.sspersistservice.model.Equipment;
import com.hz.sspersistservice.model.EquipmentPersistSystemInfos;
import com.hz.sspersistservice.repository.EquipmentRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("equipment")
public class EquipmentController {

    private static final Log LOGGER = LogFactory.getLog(EquipmentController.class);
    RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private EquipmentRepository equipmentRepository;

    @GetMapping("all")
    public List<Equipment> getAllEquipment(){
        return equipmentRepository.findAll();
    }

    @GetMapping("{id}")
    public Equipment getEquipmentById(@PathVariable("id") int id){
        return equipmentRepository.findById(id);
    }

    @PostMapping("add")
    public Equipment saveEquipement(@RequestBody Equipment equipment){
        return equipmentRepository.save(equipment);
    }

    @PostMapping("infos")
    public EquipmentPersistSystemInfos getInfosFromOderService(@RequestBody EquipmentPersistSystemInfos equipmentPersistSystemInfos){
        LOGGER.info("#### start receiving information ####");
        LOGGER.info("#### From: "+equipmentPersistSystemInfos.getFrom());
        LOGGER.info("#### Service Id: "+equipmentPersistSystemInfos.getServiceId());
        LOGGER.info("#### Service Id: "+equipmentPersistSystemInfos.getIsValid());
        LOGGER.info("#### End receiving information ####");
        return equipmentPersistSystemInfos;
    }

    @GetMapping("test")
    public String apiTest(){
        return "Equipement Persist service work well";
    }
}

