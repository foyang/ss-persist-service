package com.hz.sspersistservice.repository;


import com.hz.sspersistservice.model.Equipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentRepository extends JpaRepository<Equipment, Integer> {
    Equipment findById(int id);
}
