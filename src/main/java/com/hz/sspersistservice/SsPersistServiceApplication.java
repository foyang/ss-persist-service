package com.hz.sspersistservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsPersistServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsPersistServiceApplication.class, args);
	}

}
