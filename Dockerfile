FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/ss-persist-service-0.0.1-SNAPSHOT.jar ss-persist-service-0.0.1-SNAPSHOT.jar
EXPOSE 9002
COPY . .
ENTRYPOINT ["java", "-jar","target/ss-persist-service-0.0.1-SNAPSHOT.jar"]